public class Circle{
    int radius;
    Point center;
    public Circle(Point c, int r){
	this.radius = r;
	this.center = c;
    }
    public double area(){
        return 3.14*Math.pow(this.radius,2);
    }
    public double perimeter(){
        return 2*3.14*this.radius;
    }
    public boolean intersect(Circle c){
	double distance = Math.sqrt(Math.pow(this.center.xCoord-c.center.xCoord,2)+Math.pow(this.center.yCoord-c.center.yCoord,2));
	
	if (distance < this.radius+c.radius)
		return true;
	else
		return false;
	}
     
}
        
        
