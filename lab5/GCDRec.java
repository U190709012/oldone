public class GCDRec{
    public static void main(String[] args){
        int x1 = Integer.parseInt(args[0]);
        int x2 = Integer.parseInt(args[1]);
        System.out.println(gcd(x1,x2));
        
    }
    public static int gcd(int x1, int x2){
        if(x2==0)
            return x1;
        return gcd(x2,x1 % x2);
    }
}
