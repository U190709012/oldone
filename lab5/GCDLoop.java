public class GCDLoop{
    public static void main(String[] args){
        int x1 = Integer.parseInt(args[0]);
        int x2 = Integer.parseInt(args[1]);
        System.out.println(gcd(x1,x2));
        
    }
    public static int gcd(int x1, int x2){
        int r;
        r = x1% x2;
        x1 = x2;
        x2 = r;
        return x1;
    }
}
